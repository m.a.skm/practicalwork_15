﻿#include <iostream>
using namespace std;
const int N = 20;

void NumberPrinting (int a, int b) 
{	
	int d = a % 2;
		if (d == 0 && a <= b)
		{
			cout << "You entered an even number: " << a << endl;
		}
		else if (d != 0 && a <= b)
		{
			cout << "You entered an odd number: " << a << endl;
		}
		else 
		{
			cout << "You entered a number greater than " << N << endl;
		}
		
}

int main()
{
	/*int X = 0;	
	  while (X <= N)	
	  {
		++X;		
		int Y = X % 2;				
		if (Y == 0) 
		{
			cout << X << endl;
		}		
	}*/

	for (size_t i = 0; i <= N; ++i)
	{
		int x = i % 2;
		if (x == 0)
		{
			cout << i << endl;
		}		
	}

	int c = 0;
	cout << "Enter an integer from 0 to " << N << endl;
	cin >> c;
	NumberPrinting (c, N);
	return 0;
}


